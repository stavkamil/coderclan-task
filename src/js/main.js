window.onload = function () {
  gsap.registerPlugin(ScrollTrigger);

  //   functions
  const fadeAnimation = (el) => {
    gsap.from(el, {
      scrollTrigger: el,
      x: -50,
      duration: 1,
      opacity: 0,
    });
  };

  const hoverEffect = (parent, icon) => {
    document.querySelector(parent).addEventListener("mouseenter", (e) => {
      gsap.to(icon, {
        scaleX: 1.5,
        scaleY: 1.5,
        duration: 0.5,
      });
    });

    document.querySelector(parent).addEventListener("mouseleave", (e) => {
      gsap.to(icon, {
        scaleX: 1,
        scaleY: 1,
        duration: 0.5,
      });
    });
  };

  // function exection
  fadeAnimation(".hero");
  fadeAnimation(".feature.first");
  fadeAnimation(".feature.second");
  fadeAnimation(".card.one");
  fadeAnimation(".card.two");
  fadeAnimation(".card.three");

  hoverEffect(".card__icon.one", "#lock");
  hoverEffect(".card__icon.two", "#pizza");
  hoverEffect(".card__icon.three", "#thumbsDown");
};
